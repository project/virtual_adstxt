<?php

namespace Drupal\virtual_adstxt\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AdminAdstxtForm.
 *
 * @package Drupal\virtual_adstxt\Form
 */
class AdminAdstxtForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'virtual_adstxt_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'virtual_adstxt.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('virtual_adstxt.settings');

    $form['ads_txt'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Ads.txt entry'),
      '#rows' => 20,
      '#cols' => 6,
      '#default_value' => $config->get('ads_txt'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('virtual_adstxt.settings');
    $config->set('ads_txt', $form_state->getValue('ads_txt'));
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
