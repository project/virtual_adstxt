<?php

namespace Drupal\virtual_adstxt\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller for our ads.txt route.
 */
class AdstxtController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'virtual_adstxt.settings',
    ];
  }

  /**
   * Returns ads txt text.
   */
  public function show() {
    $config = $this->config('virtual_adstxt.settings');
    $response = new Response($config->get('ads_txt'), 200, []);
    $response->headers->set('Content-Type', 'text/plain');
    return $response;
  }

}
