CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Creating your own ads.txt file gives you more control over who's allowed to sell ads on your site and helps prevent counterfeit inventory from being presented to advertisers.
The module provides a text area to edit the ads.txt file.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/virtual_adstxt

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/virtual_adstxt



INSTALLATION
------------

 * Install the Virtual Ads.txt as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > configuration > Edit Ads.txt
    3. Clear your browser's cache.

MAINTAINERS
-----------

Module created by:
 *  Vishnu Kumar - https://www.drupal.org/u/vishnukumar
